<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLpProjects extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp_projects', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('name', 100);
            $table->string('description', 1000);
            $table->dateTime('date_start');
            $table->dateTime('date_finish')->nullable();
            $table->dateTime('date_add');
            $table->dateTime('date_end')->nullable();
            $table->boolean('is_done')->default(0);
            $table->string('extra', 50);
            
            $table->foreign('category_id')->references('id')->on('tazaq_lp_categories');
        });
    }
    
    public function down()
    {    
        Schema::dropIfExists('tazaq_lp_projects');
    }
}