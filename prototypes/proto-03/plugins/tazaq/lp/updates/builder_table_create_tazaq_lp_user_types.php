<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLpUserTypes extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp_user_types', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('position', 100);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp_user_types');
    }
}