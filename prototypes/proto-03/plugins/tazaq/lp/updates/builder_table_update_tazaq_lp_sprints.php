<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTazaqLpSprints extends Migration
{
    public function up()
    {
        Schema::table('tazaq_lp_sprints', function($table)
        {
            $table->integer('f_project')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('tazaq_lp_sprints', function($table)
        {
            $table->dropColumn('f_project');
        });
    }
}
