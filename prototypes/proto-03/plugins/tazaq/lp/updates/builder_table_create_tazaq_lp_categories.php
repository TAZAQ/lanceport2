<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLpCategories extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 50);
            $table->string('description', 100);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp_categories');
    }
}
