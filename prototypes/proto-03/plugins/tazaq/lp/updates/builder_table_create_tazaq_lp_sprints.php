<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLpSprints extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp_sprints', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->string('name', 100);
            $table->string('target', 1000);
            $table->dateTime('date_start');
            $table->dateTime('date_finish')->nullable();
            
            $table->foreign('project_id')->references('id')->on('tazaq_lp_projects');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp_sprints');
    }
}