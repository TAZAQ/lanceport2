<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLpTasks extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp_tasks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('task_type_id')->unsigned();
            $table->integer('priority_id')->unsigned();
            $table->integer('task_id')->nullable()->unsigned();
            $table->integer('lpuser_id')->unsigned();
            $table->string('name', 100);
            $table->string('description', 200);
            $table->decimal('estimate', 10, 2)->default(10.2);
            $table->text('result_text')->nullable();
            $table->boolean('in_report')->default(0);
            $table->string('extra', 50)->nullable();
            
            $table->foreign('project_id')->references('id')->on('tazaq_lp_projects');
            $table->foreign('task_type_id')->references('id')->on('tazaq_lp_task_types');
            $table->foreign('priority_id')->references('id')->on('tazaq_lp_priorities');
            $table->foreign('task_id')->references('id')->on('tazaq_lp_tasks');
            $table->foreign('lpuser_id')->references('id')->on('tazaq_lp_lpusers');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp_tasks');
    }
}