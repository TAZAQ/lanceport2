<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLpTaskTags extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp_task_tags', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('task_id')->unsigned();
            $table->string('name', 50);
            
            $table->foreign('task_id')->references('id')->on('tazaq_lp_tasks');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp_task_tags');
    }
}