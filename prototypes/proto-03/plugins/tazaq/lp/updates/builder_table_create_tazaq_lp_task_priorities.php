<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLpTaskPriorities extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp_task_priorities', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 30);
            $table->integer('level')->unsigned()->default(0);
            $table->string('css', 30);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp_task_priorities');
    }
}
