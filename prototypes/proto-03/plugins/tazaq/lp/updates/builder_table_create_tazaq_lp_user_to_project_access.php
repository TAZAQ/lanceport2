<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLpUserToProjectAccess extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp_user_to_project_access', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('f_user')->unsigned();
            $table->integer('f_project')->unsigned();
            $table->boolean('on_projects')->default(0);
            $table->boolean('on_sprints')->default(0);
            $table->boolean('on_tasks')->default(1);
            $table->boolean('on_report')->default(1);
            $table->primary(['f_user','f_project']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp_user_to_project_access');
    }
}
