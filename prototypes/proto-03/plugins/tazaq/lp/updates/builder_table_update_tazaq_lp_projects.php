<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTazaqLpProjects extends Migration
{
    public function up()
    {
        Schema::table('tazaq_lp_projects', function($table)
        {
            $table->integer('f_category')->unsigned();
            $table->dateTime('date_end')->default(null)->change();
            $table->string('extra', 50)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('tazaq_lp_projects', function($table)
        {
            $table->dropColumn('f_category');
            $table->dateTime('date_end')->default('NULL')->change();
            $table->string('extra', 50)->default('NULL')->change();
        });
    }
}
