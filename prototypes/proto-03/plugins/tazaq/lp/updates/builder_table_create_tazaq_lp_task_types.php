<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLpTaskTypes extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp_task_types', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 30);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp_task_types');
    }
}
