<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLpUsers extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp_lpusers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('user_type_id')->unsigned();
            $table->string('name', 50);
            $table->string('initials', 3);
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('user_type_id')->references('id')->on('tazaq_lp_user_types');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp_lpusers');
    }
}