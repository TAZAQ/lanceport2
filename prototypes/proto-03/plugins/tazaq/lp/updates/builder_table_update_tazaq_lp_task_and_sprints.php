<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTazaqLpTaskAndSprints extends Migration
{
    public function up()
    {
        Schema::table('tazaq_lp_task_and_sprints', function($table)
        {
            $table->primary(['f_task','f_sprint']);
        });
    }
    
    public function down()
    {
        Schema::table('tazaq_lp_task_and_sprints', function($table)
        {
            $table->dropPrimary(['f_task','f_sprint']);
        });
    }
}
