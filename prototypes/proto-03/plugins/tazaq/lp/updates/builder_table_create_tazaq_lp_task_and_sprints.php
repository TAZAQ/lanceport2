<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTazaqLpTaskAndSprints extends Migration
{
    public function up()
    {
        Schema::create('tazaq_lp_task_and_sprints', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('f_task')->unsigned();
            $table->integer('f_sprint')->unsigned();
            $table->dateTime('date_at');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tazaq_lp_task_and_sprints');
    }
}
