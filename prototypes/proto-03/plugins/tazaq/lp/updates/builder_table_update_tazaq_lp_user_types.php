<?php namespace Tazaq\Lp\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTazaqLpUserTypes extends Migration
{
    public function up()
    {
        Schema::table('tazaq_lp_user_types', function($table)
        {
            $table->integer('f_user')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('tazaq_lp_user_types', function($table)
        {
            $table->dropColumn('f_user');
        });
    }
}
