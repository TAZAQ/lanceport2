<?php

class Task {
    const ALPHABETH = '0abcdefghi';

    const TASK_TEMPLATE = <<<EOF
<div data-orig-pos="%d" data-score="0" data-priority="0" data-date="10.05.1995" id="%s"
     class="task uk-background-secondary uk-card uk-card-default uk-card-body uk-margin-small-bottom task-priority-low">
    <div class="task-title uk-light">
        <span>Новая задача</span>
    </div>
</div>
EOF;

    /**
     * Перевод timestamp в буквоключ, например 1580922445 -> aeh0ibbdde
     * @return string
     */
    private function getTaskKey(): string {
        $current_time = (string) time();
        $task_key = "";
        for ($i = 0; $i < strlen($current_time); $i++) {
            $task_key = $task_key . self::ALPHABETH[$current_time[$i]];
        }

        return $task_key;
    }

    public function Add() {
        return sprintf(self::TASK_TEMPLATE, 12323, $this->getTaskKey() );
    }
}