<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Рабочая область</title>

    <link rel="stylesheet" href="Styles/uikit.min.css">
    <link rel="stylesheet" href="Styles/lanceportWork.css">

    <script src="Scripts/uikit.min.js"></script>
    <script src="Scripts/uikit-icons.min.js"></script>
</head>

<body data-uk-img="" data-src="Img/background.png" class="uk-flex uk-background-cover" uk-height-viewport>
    <div class="uk-overlay-primary uk-position-cover"></div>


    <?php include 'MainBlocks/Work/header.php'?>

    <?php include 'MainBlocks/Work/main.php'?>

    <?php include 'MainBlocks/Work/offsetProject.php'?>


    <!--  модальные окна  -->
    <?php include 'Modals/WorkModals/modalUserProfile.php'?>
    <?php include 'Modals/WorkModals/modalProjectNew.php'?>


    <script src="Scripts/jquery-3.4.1.min.js"></script>
    <script src="Scripts/lanceportCommon.js"></script>
    <script src="Scripts/lanceportWork.js"></script>
</body>
</html>