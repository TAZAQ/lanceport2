<div class="uk-comment uk-comment-primary uk-background-secondary uk-padding-small uk-margin-small-bottom">
    <div class="uk-comment-header uk-grid-small uk-flex-middle uk-margin-remove-bottom">
        <div class="uk-width-auto">
            <div class="user-info uk-margin-bottom">
                <div class="user-info-avatar lp-btn">A</div>
                <div class="user-info-name uk-text-muted">Author</div>
                <a class="notify-close uk-alert-close" uk-close></a>
            </div>
        </div>
    </div>

    <div class="uk-comment-body">
        <span>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
            et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
            Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
        </span>
    </div>
</div>