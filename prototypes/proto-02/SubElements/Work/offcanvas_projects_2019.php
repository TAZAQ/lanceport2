<a href="/project-1" class="offcanvas-project uk-box-shadow-large uk-background-secondary uk-card uk-card-default uk-padding-remove uk-margin-small-bottom uk-flex">
    <div class="offcanvas-project-image uk-flex-middle">
        <img src="Img/wp.png" alt="wp">
    </div>
    <div class="uk-card-body uk-padding-small uk-text-middle">
        WP шаблон
    </div>
</a>

<a href="/project-2" class="offcanvas-project uk-box-shadow-large uk-background-secondary uk-card uk-card-default uk-padding-remove uk-margin-small-bottom uk-flex">
    <div class="offcanvas-project-image uk-flex-middle">
        <img src="Img/vk.png" alt="vk">
    </div>
    <div class="uk-card-body uk-padding-small uk-text-middle">
        Настроить группу ВК
    </div>
</a>