<div data-orig-pos="10000" data-score="5" data-priority="0" data-date="10.05.1995" id="10000"
     class="task uk-background-secondary uk-card uk-card-default uk-card-body uk-margin-small-bottom task-priority-critical">
    <div class="task-title uk-light">
        <span>Вырастить дом</span>
        <span class="task-counter uk-float-right">0/10</span>
        <span class="icon-fix uk-float-right" uk-icon="list"></span>
    </div>
    <div class="task-items uk-text-right non-select">
        <span class="uk-badge task-dates">09.05.1995 - 10.05.1995</span>
        <span class="uk-badge task-scores">5</span>
    </div>
</div>

<div data-orig-pos="10001" data-score="10" data-priority="1" data-date="11.05.1995" id="10001"
     class="task uk-background-secondary uk-card uk-card-default uk-card-body uk-margin-small-bottom task-priority-high">
    <div class="task-title uk-light">
        <span>Построить дерево</span>
        <span class="task-counter uk-float-right">0/10</span>
        <span class="icon-fix uk-float-right" uk-icon="list"></span>
    </div>
    <div class="task-items uk-text-right non-select">
        <span class="uk-badge task-dates">09.05.1995 - 11.05.1995</span>
        <span class="uk-badge task-scores">10</span>
    </div>
</div>

<div data-orig-pos="10002" data-score="1" data-priority="2" data-date="09.05.1995" id="10002"
     class="task uk-background-secondary uk-card uk-card-default uk-card-body uk-margin-small-bottom task-priority-mid">
    <div class="task-title uk-light">
        <span>Посадить сына</span>
        <span class="task-counter uk-float-right">0/10</span>
        <span class="icon-fix uk-float-right" uk-icon="list"></span>
    </div>
    <div class="task-items uk-text-right non-select">
        <span class="uk-badge task-dates">09.05.1995 - 09.05.1995</span>
        <span class="uk-badge task-scores">1</span>
    </div>
</div>

<div data-orig-pos="10003" data-score="4" data-priority="3" data-date="08.05.1995" id="10003"
     class="task uk-background-secondary uk-card uk-card-default uk-card-body uk-margin-small-bottom task-priority-low">
    <div class="task-title uk-light">
        <span>Покушать</span>
        <span class="task-counter uk-float-right">0/10</span>
        <span class="icon-fix uk-float-right" uk-icon="list"></span>
    </div>
    <div class="task-items uk-text-right non-select">
        <span class="uk-badge task-dates">07.05.1995 - 08.05.1995</span>
        <span class="uk-badge task-scores">4</span>
    </div>
</div>