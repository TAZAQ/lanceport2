<?php

// реакция на сообщение
function reaction($data) {
    if ($data === null) send("⛔ Пустой запрос");
    elseif ($data->action === null) send("⛔ Пустое действие");
    else {
        $action = $data->action;

        // запрос с обратной связи**************************************************************************************
        if ($action === 'feedback') {
            send("✅ Принято!");
        }

        if ($action === 'task_add') {
            include 'ServerFiles/taskAdd.php';
            $task = new Task();
            $task_result = $task->Add();

            send($task_result);
        }

        // если нет такого действия*************************************************************************************
        else {
            send("⛔ Неизвестное действие");
        }
    }

}

// отправляем обратно
function send($data) {
    echo json_encode(["data" => $data]);
}

// получаем данные
$data = file_get_contents("php://input");
$data = json_decode($data);
reaction($data);