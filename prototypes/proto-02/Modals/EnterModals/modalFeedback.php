<div id="modal_feedback" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
            <h3 class="uk-modal-title uk-text-primary">Свяжитесь с нами</h3>
        </div>
        <form class="uk-modal-body" id="feedback_form">
            <label>Имя*
                <input type="text" name="from_name" class="uk-input uk-margin-bottom" placeholder="Как к Вам обращаться?" required>
            </label>
            <label>E-mail*
                <input type="email" name="from_email" class="uk-input uk-margin-bottom" placeholder="Ваша почта..." required>
            </label>
            <label>Тема*
                <input type="text" name="from_theme" class="uk-input uk-margin-bottom" placeholder="Что Вас интересует?" required>
            </label>
            <label>Текст сообщения*
                <textarea name="from_message" class="uk-textarea uk-margin-bottom"  placeholder="Ваше сообщение..." rows="7" required style="resize: none"></textarea>
            </label>
            <span>Или: <a href="mailto:tazaqsp@mail.ru">tazaqsp@mail.ru</a></span>
        </form>
        <div class="uk-modal-footer uk-text-right">
            <span id="feedback_returned_message"></span>
            <button class="uk-button uk-button-default uk-modal-close" type="button">Отмена</button>
            <button class="uk-button uk-button-primary" type="button" id="btn_send_feedback">Отправить</button>
        </div>
    </div>
</div>