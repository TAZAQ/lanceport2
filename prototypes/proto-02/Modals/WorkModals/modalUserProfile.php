<div id="modal_user_profile" uk-modal>
    <div class="uk-modal-dialog uk-width-1-2 lp-modal-bg">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-body">

            <div class="user-info uk-margin-bottom">
                <div class="user-info-avatar lp-btn">ЧВ</div>
                <div class="user-info-name uk-text-muted"">Чеснов Владислав</div>
            </div>

            <div class="uk-flex">
                <div class="uk-width-1-3">
                    <ul class="uk-nav uk-light">
                        <li class="uk-link mup_mi" uk-toggle="target: .user-notify-container">Уведомления</li>
                        <li class="uk-link mup_mi" uk-toggle="target: .user-data">Данные пользователя</li>
                        <li class="uk-link mup_mi" uk-toggle="target: .user-data-security">Конфиденциальность</li>
                    </ul>
                </div>

                <div class="uk-width-2-3">
                    <fieldset class="user-notify-container uk-fieldset uk-light uk-margin-bottom m_user_profile_fs uk-animation-slide-left-small">
                        <?php include "SubElements/Work/ModalUserProfile/newMessages.php"?>
                    </fieldset>

                    <form class="user-data uk-fieldset uk-light uk-margin-bottom m_user_profile_fs uk-animation-slide-left-small" hidden>
                        <label>
                            <span>ФИО</span>
                            <input type="text" class="uk-input uk-width uk-margin-small-bottom" name="user_full_name" placeholder="Имя для чата, например Сергей Михайлов">
                        </label>
                        <label>
                            <span>Инициалы</span>
                            <input type="text" class="uk-input uk-width uk-margin-small-bottom" name="user_initials" placeholder="Инициалы для аватара">
                        </label>
                        <label>
                            <span>E-mail</span>
                            <input type="text" class="uk-input uk-width uk-margin-small-bottom" name="user_email" placeholder="E-mail для связи">
                        </label>
                        <button type="button" class="uk-button uk-button-default uk-margin-small-top uk-float-right" id="btn-user-data">Сохранить</button>
                    </form>

                    <form class="user-data-security uk-fieldset uk-light m_user_profile_fs uk-animation-slide-left-small" hidden>
                        <label>
                            <span>Старый пароль</span>
                            <input type="text" class="uk-input uk-width uk-margin-small-bottom" name="user_old_pass" placeholder="Введите старый пароль">
                        </label>
                        <label>
                            <span>Новый пароль</span>
                            <input type="text" class="uk-input uk-width uk-margin-small-bottom" name="user_new_pass" placeholder="Введите новый пароль">
                        </label>
                        <label>
                            <span>Повторите пароль</span>
                            <input type="text" class="uk-input uk-width uk-margin-small-bottom" name="user_new_pass2" placeholder="Повторите новый пароль">
                        </label>
                        <button type="button" class="uk-button uk-button-default uk-margin-small-top uk-float-right" id="btn-user-data-security">Сохранить</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>