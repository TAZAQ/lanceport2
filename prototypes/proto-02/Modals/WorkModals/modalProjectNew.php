<div id="modal_project_edit" uk-modal>
    <div class="uk-modal-dialog lp-modal-bg uk-light uk-box-shadow-large">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header lp-modal-bg">
            <span class="uk-h4">Изменить проект</span>
        </div>


        <form class="uk-modal-body" id="form_modal_project_edit">
            <!-- Название проекта -->
            <label>
                <span class="non-select"><i class="icon-fix-modal" uk-icon="file-edit"></i>Название проекта </span>
                <input type="text" class="uk-input uk-margin-small-bottom" name="project_name" maxlength="50" placeholder="Максимум 50 символов">
            </label>

            <!-- Описание проекта -->
            <label class="uk-margin-small-bottom">
                <span class="non-select"><i class="icon-fix-modal" uk-icon="file-text"></i>Описание проекта</span>
                <textarea name="project_description" class="uk-textarea uk-margin-small-bottom" cols="35" rows="5" style="resize: none" placeholder="Максимум 1000 символов"></textarea>
            </label>

            <!-- Категория -->
            <label class="uk-margin-small-bottom">
                <span class="non-select"><i class="icon-fix-modal" uk-icon="list"></i>Категория</span>
                <select class="uk-select lp-modal-bg uk-margin-small-bottom" name="project_category">
                    <option value="0">Без категории</option>
                    <option value="1">Категория 1</option>
                    <option value="2">Категория 2</option>
                </select>
            </label>

            <!-- Сроки -->
            <div class="uk-flex">
                <div class="uk-width-1-2 uk-margin-small-right">
                    <label><i class="icon-fix-modal" uk-icon="calendar"></i>Начало<input type="date" class="uk-input uk-margin-small-bottom" name="project_date_start"></label>
                </div>
                <div class="uk-width-1-2 uk-margin-small-left">
                    <label>Окончание<input type="date" class="uk-input uk-margin-small-bottom" name="project_date_finish"></label>
                </div>
            </div>

            <!-- Метки -->
            <label class="uk-margin-small-bottom">
                <span class="non-select"><i class="icon-fix-modal" uk-icon="tag"></i>Метки</span>
                <textarea name="project_tags" class="uk-textarea uk-margin-small-bottom" cols="35" rows="5" style="resize: none" placeholder="Метки через запятую"></textarea>
            </label>
        </form><!-- /.uk-modal-body -->


        <div class="uk-modal-footer lp-modal-bg">
            <div class="uk-align-left uk-margin-remove-bottom">
                <button type="button" class="uk-button uk-button-danger">Завершить</button>
            </div>
            <div class="uk-align-right uk-margin-remove-bottom">
                <button type="button" class="uk-button uk-button-default">Отмена</button>
                <button type="button" class="uk-button uk-button-primary" id="btn_task_save">Изменить</button>
            </div>
        </div>
    </div>
</div>