<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Вход</title>

    <link rel="stylesheet" href="Styles/uikit.min.css">
    <link rel="stylesheet" href="Styles/lanceportEnter.css">

    <script src="Scripts/uikit.min.js"></script>
    <script defer src="Scripts/uikit-icons.min.js"></script>
</head>
<body>

    <div data-uk-img="" data-src="Img/background.png" class="uk-flex uk-flex-center uk-flex-middle uk-background-cover" uk-height-viewport>
        <div class="uk-overlay-primary uk-position-cover"></div>
        <div class="uk-overlay uk-position-center uk-light tzq-auth-form">

            <div class="uk-flex uk-flex-row uk-flex-center">
                <h3></h3>
                <h3 class="uk-text-muted">free</h3>
                <span class="uk-heading-small">Lance</span>
                <h3 class="uk-text-muted">re</h3>
                <span class="uk-heading-small">Port</span>
            </div>

            <form>
                <div class="uk-margin">
                    <div class="uk-inline uk-flex">
                        <span class="uk-form-icon" uk-icon="icon: user"></span>
                        <input class="uk-input uk-flex-1" type="text" placeholder="Логин">
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-inline uk-flex">
                        <span class="uk-form-icon" uk-icon="icon: lock"></span>
                        <input class="uk-input" type="password" placeholder="Пароль">
                    </div>
                </div>
                <div class="uk-margin">
                    <button class="uk-button uk-button-default">Войти</button>

                    <a href="#modal_feedback" class="uk-button" uk-toggle>Обратная связь</a>
                </div>
            </form>
        </div>
    </div>

    <!-- Модальное окно обратной связи-->
    <?php include "Modals/EnterModals/modalFeedback.php"; ?>

    <script src="Scripts/jquery-3.4.1.min.js"></script>
    <script src="Scripts/lanceportEnter.js"></script>
</body>
</html>
