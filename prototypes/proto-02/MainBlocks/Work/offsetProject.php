<!--  Offcanvas панель с проектами  -->
<div id="all_projects" uk-offcanvas>
    <div class="uk-offcanvas-bar uk-padding-small uk-box-shadow-large">
        <div class="user-info uk-padding-small" uk-toggle="target: #modal_user_profile">
            <div class="user-info-avatar lp-btn" id="user_avatar">ЧВ</div>
            <div class="user-info-name lp-btn uk-link" id="user_name">Чеснов Владислав</div>
        </div>
        <div class="uk-flex uk-flex-column uk-padding-small uk-margin-small-bottom">
            <button class="uk-button uk-button-default uk-margin-small-bottom">Настроить категории</button>
            <button class="uk-button uk-button-default uk-margin-small-bottom uk-text-success">Создать проект</button>
        </div>
        <h3 class="uk-margin-small">Проекты</h3>
        <div class="project_container">
            <ul uk-accordion="multiple: true">
                <li class="uk-open">
                    <span class="uk-accordion-title">2019</span>
                    <div class="uk-accordion-content">
                        <?php include "SubElements/Work/offcanvas_projects_2019.php"?>
                    </div>
                </li>
                <li>
                    <span class="uk-accordion-title">2018</span>
                    <div class="uk-accordion-content">
                    </div>
                </li>
                <li>
                    <span class="uk-accordion-title">2017</span>
                    <div class="uk-accordion-content">
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>