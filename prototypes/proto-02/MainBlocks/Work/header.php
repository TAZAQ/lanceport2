<header class="uk-background-secondary">
    <nav class="uk-background-secondary uk-light uk-padding-small" uk-navbar>
        <div class="uk-navbar-left">
            <button class="uk-button uk-button-link uk-flex uk-text-baseline" type="button" uk-toggle="target: #all_projects">
                <span uk-icon="thumbnails"></span>
                <span>Все проекты</span>
            </button>

            <button class="uk-button uk-button-link uk-flex uk-text-baseline" type="button" uk-toggle="target: #modal_project_edit">
                <span class="uk-margin-left" uk-icon="settings"></span>
                <span id="id_project_title">Название проекта</span>
            </button>
        </div>



        <div class="uk-navbar-right uk-margin-large-right">
            <span class="uk-text-muted">Видимость:</span>

            <div class="uk-margin-left">
                <button class="uk-button uk-button-link uk-text-primary" type="button">Вид задач</button>
                <div class="uk-background-secondary uk-box-shadow-large" uk-dropdown>
                    <div class="uk-flex uk-flex-column" id="lp_tasks_view">
                        <label class="uk-text-primary uk-margin-small-bottom">
                            <input class="uk-radio lp-task-view-radio" type="radio" name="tasks_view_type" id="tasks_view_0" checked> Полный
                        </label>
                        <label class="uk-text-primary uk-margin-small-bottom">
                            <input class="uk-radio lp-task-view-radio" type="radio" name="tasks_view_type" id="tasks_view_1"> Компактный
                        </label>
                    </div>
                </div>
            </div>



            <div class="uk-margin-left">
                <button class="uk-button uk-button-link uk-text-primary" type="button">Сортировки</button>
                <div class="uk-background-secondary uk-box-shadow-large" uk-dropdown>
                    <div class="uk-flex uk-flex-column" id="lp_sortings">
                        <label class="uk-text-primary uk-margin-small-bottom">
                            <input class="uk-radio lp-sorts-radio" type="radio" name="sort_type" id="sort_type_0" checked> По умолчанию
                        </label>
                        <label class="uk-text-primary uk-margin-small-bottom">
                            <input class="uk-radio lp-sorts-radio" type="radio" name="sort_type" id="sort_type_1"> По важности
                        </label>
                        <label class="uk-text-primary uk-margin-small-bottom">
                            <input class="uk-radio lp-sorts-radio" type="radio" name="sort_type" id="sort_type_2"> По дате завершения
                        </label>
                        <label class="uk-text-primary uk-margin-small-bottom">
                            <input class="uk-radio lp-sorts-radio" type="radio" name="sort_type" id="sort_type_3"> По очкам
                        </label>
                    </div>
                </div>
            </div>



            <div class="uk-margin-left">
                <button class="uk-button uk-button-link uk-text-primary" type="button">Фильтры</button>
                <div class="uk-background-secondary uk-box-shadow-large" uk-dropdown>
                    <div class="uk-flex uk-flex-column" id="lp_filters">
                        <label class="uk-text-danger uk-margin-small-bottom"><input class="uk-checkbox lp-filter-cbox" type="checkbox" checked> Фильтр: срочно</label>
                        <label class="uk-text-warning uk-margin-small-bottom"><input class="uk-checkbox lp-filter-cbox" type="checkbox" checked> Фильтр: высокий</label>
                        <label class="uk-text-success uk-margin-small-bottom"><input class="uk-checkbox lp-filter-cbox" type="checkbox" checked> Фильтр: средний</label>
                        <label class="filter-low-text-color"><input class="uk-checkbox lp-filter-cbox" type="checkbox" checked> Фильтр: низкий</label>
                    </div>
                </div>
            </div>
        </div><!-- Правая часть -->
    </nav>
</header>