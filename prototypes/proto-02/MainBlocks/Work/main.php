<main class="uk-inline uk-light states-container" uk-filter="target: .task-container">
    <!--    Состояния задачи    -->
    <div class="uk-active" id="lp_filters_settings" uk-filter-control="sort: data-orig-pos;" hidden></div>

    <!--    Все задачи    -->
    <div class="uk-card uk-card-default uk-light uk-card-body state uk-padding-small uk-padding-remove-top">
        <div class="counter-stl uk-card-badge uk-label non-select" id="state_1_counter">0</div>
        <h3 class="uk-text-middle uk-margin-small non-select uk-text-muted">Все задачи
            <span class="lp-btn icon-fix" uk-icon="plus-circle" id="btn_task_add"></span>
        </h3>

        <hr class="uk-hr uk-margin-small">
        <div class="task-container js-filter" uk-sortable="group: task-container" cls-placeholder="task-cls-placeholder" id="state_1">
            <?php include "SubElements/Work/tasks.php"?>
        </div>
    </div>

    <!--    Сделаю    -->
    <div class="uk-card uk-card-default uk-light uk-card-body state uk-padding-small uk-padding-remove-top">
        <div class="counter-stl uk-card-badge uk-label non-select" id="state_2_counter">0</div>
        <h3 class="uk-text-middle uk-margin-small non-select uk-text-muted">Сделаю</h3>
        <hr class="uk-hr uk-margin-small">
        <div class="task-container" uk-sortable="group: task-container" cls-placeholder="task-cls-placeholder" id="state_2">

        </div>
    </div>

    <!--    В работе    -->
    <div class="uk-card uk-card-default uk-light uk-card-body state uk-padding-small uk-padding-remove-top">
        <div class="counter-stl uk-card-badge uk-label non-select" id="state_3_counter">0</div>
        <h3 class="uk-text-middle uk-margin-small non-select uk-text-muted">В работе</h3>
        <hr class="uk-hr uk-margin-small">
        <div class="task-container" uk-sortable="group: task-container" cls-placeholder="task-cls-placeholder" id="state_3">

        </div>
    </div>

    <!--    Тестирование    -->
    <div class="uk-card uk-card-default uk-light uk-card-body state uk-padding-small uk-padding-remove-top">
        <div class="counter-stl uk-card-badge uk-label non-select" id="state_4_counter">0</div>
        <h3 class="uk-text-middle uk-margin-small non-select uk-text-muted">Тестирование</h3>
        <hr class="uk-hr uk-margin-small">
        <div class="task-container" uk-sortable="group: task-container" cls-placeholder="task-cls-placeholder" id="state_4">

        </div>
    </div>

    <!--    Готово    -->
    <div class="uk-card uk-card-default uk-light uk-card-body state uk-padding-small uk-padding-remove-top">
        <div class="counter-stl uk-card-badge uk-label non-select" id="state_5_counter">0</div>
        <h3 class="uk-text-middle uk-margin-small non-select uk-text-muted">Готово</h3>
        <hr class="uk-hr uk-margin-small">
        <div class="task-container" uk-sortable="group: task-container" cls-placeholder="task-cls-placeholder" id="state_5">

        </div>
    </div>

</main>