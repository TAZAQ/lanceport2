mainWorkJS();

function mainWorkJS() {
    // обновление счётчиков
    onTaskMoved();

    // фильтры
    onFilterAndSortsClicks();

    // внешний вид задач (минимизация)
    onTasksViewChange();

    // слушатели на кнопки
    onBtnClickListeners();
}

// обновление счётчиков
function onTaskMoved() {
    const id_counter_text = '_counter';
    const id_state_text = 'state_';


    try {
        // инициализация счётчиков
        initCounters();

        // инициализация счётчиков
        function initCounters(counter_num=5) {
            for (let counter_item = 1; counter_item <= counter_num; counter_item++) {
                let state_id = '#' + id_state_text + counter_item;
                let counter_id = state_id + id_counter_text;
                $(counter_id).text( $(state_id)[0].childElementCount );
            }
        }

        // при добавлении
        UIkit.util.on('.task-container', 'added', function (item) {
            let to_state_id = '#' + item['target'].id;
            let to_state_counter_id = to_state_id + id_counter_text;

            $(to_state_counter_id).text(
                $(to_state_id)[0].childElementCount
            );
        });

        // при удалении
        UIkit.util.on('.task-container', 'removed', function (item) {
            let from_state_id = '#' + item['target'].id;
            let from_state_counter_id = from_state_id + id_counter_text;

            $(from_state_counter_id).text(
                $(from_state_id)[0].childElementCount
            );
        });
    }
    catch (e) {}

}

// реализация фильтров
function onFilterAndSortsClicks() {
    try {
        // сортировка по умолчанию
        const sort_default = 'sort: data-orig-pos';

        // глобальные
        let method_filter_string = "";
        let method_sorting_string = sort_default;

        // общий метод для постановки атрибута
        function setFilterAndSort() {
            // uikit класс фильтра
            const filter_attribute = "uk-filter-control";

            // итоговый фильтр
            let full_filter = "";
            if (method_filter_string === '') {
                full_filter = method_sorting_string;
            } else {
                full_filter = method_filter_string + ';' + method_sorting_string;
            }

            // установка фильтра
            lp_filters_settings.setAttribute(filter_attribute, full_filter);

            // костыль =)
            $('#lp_filters_settings').trigger('click');
        }

        // установить сортировку
        function setSort(value=sort_default) {
            method_sorting_string = value;
        }

        // сбросить сортировку
        function cancelSort() {
            setSort();
        }

        // для фильтров*************************************************************************************************
        $('.lp-filter-cbox').click(function () {
            const filters_names = [
                ".task-priority-critical",
                ".task-priority-high",
                ".task-priority-mid",
                ".task-priority-low",
            ];

            // сборка фильтра
            let filter_string = "";
            let filters = $('.lp-filter-cbox');
            for (let filter=0; filter < filters.length; filter++) {
                if (filters[filter].checked) {
                    filter_string += filters_names[filter] + ',';
                }
            }

            // запоминаем фильтры
            method_filter_string = 'filter:' + filter_string.slice(0, -1);

            // устанавливаем фильтр
            setFilterAndSort();
        });

        // для сортировок***********************************************************************************************
        $('.lp-sorts-radio').click(function () {
            const sorts_obj = {
                'data-priority': 'sort: data-priority; order: desc',
                'data-date':     'sort: data-date;',
                'data-score':    'sort: data-score; order: desc',
            };

            // получение типа сортировки
            let sort_type = this.id;
            switch (sort_type) {
                case 'sort_type_1':
                    setSort(sorts_obj["data-priority"]);
                    break;
                case 'sort_type_2':
                    setSort(sorts_obj["data-date"]);
                    break;
                case 'sort_type_3':
                    setSort(sorts_obj["data-score"]);
                    break;
                default:
                    cancelSort();
                    break;
            }

            setFilterAndSort();
        });
    }
    catch (e) {}
}

// реализация минимизации задач
function onTasksViewChange() {
    try {
        $('input[name=tasks_view_type]').change(function () {
            if (this.id === 'tasks_view_0') {
                $('.task-items').show();
            } else {
                $('.task-items').hide();
            }
        });
    }
    catch (e) {}
}

// все кнопки
function onBtnClickListeners() {
    // новая задача
    btn_task_add.onclick = onTaskAddClick;
    // управление меню в профиле пользователя
    $('.mup_mi').on('click', onModalUserProfileMenuItemClick);
    // удаление уведомления
    $('.notify-close').on('click', onNotifyCloseClick);
}

// реализация добавления
async function onTaskAddClick() {
    const action = 'task_add';

    let result = await fetchSend(action);
    if (result.status !== STATUS_ERR) {
        $('#state_1').append(result.data);
        $('#lp_filters_settings').trigger('click');
        onTaskMoved();
    } else {

    }
}

// реализация меню в modal_user_profile
function onModalUserProfileMenuItemClick() {
    const attr_hidden = 'hidden';

    // скрываю всё
    $('.m_user_profile_fs').attr(attr_hidden, attr_hidden);

    // получаю меню-итем и забираю у него класс, который надо показать
    let atr = this.getAttribute('uk-toggle');
    let showed_class = atr.slice( atr.indexOf('.') );

    // показываю класс
    $(showed_class).removeAttr(attr_hidden);
}

// реализация удаления уведомлений
function onNotifyCloseClick() {
    $(this).parents('.uk-comment').remove();
    // TODO сделать отправку на сервер, что прочитано
}