const STATUS_OK = 'ok';
const STATUS_ERR = 'error';

// вывод в консоль
function tlg(text) {
    console.log(text);
}

// нормальная сериализация формы
function serializedArrayToObject(form_data) {
    let obj = {};
    $.each(form_data, function(i, v) { obj[v.name] = v.value; });
    return obj;
}

// отправка запроса на сервер
async function fetchSend(action, my_data=undefined) {
    let data = {
        "action": action,
        "data": my_data
    };

    let response = await fetch('serverEnter.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });

    if (response.ok) {
        return response.json();
    } else {
        return {'status': STATUS_ERR, 'data': response.statusText}
    }
}

// раскрашивание аватара
function stringToColor(str) {
    const default_color = '333333';

    let hash = 0;
    let color = '#';

    if (!str) {
        return color + default_color;
    }

    let strLength = str.length;

    for (let i = 0; i < strLength; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }

    for (let i = 0; i < 3; i++) {
        let value = (hash >> (i * 8)) & 0xFF;
        color += ('00' + value.toString(16)).substr(-2);
    }

    return color;
}