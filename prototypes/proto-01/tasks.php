<div class="card p-2 d-flex flex-column task" id="1" data-toggle="modal" data-target="#task_editor">
    <div class="task-title">Вырастить дом</div>
    <div class="task-items text-right">
        <span class="badge badge-pill badge-secondary">%s - %s</span>
        <span class="badge badge-pill badge-dark">%s</span>
        <span class="badge badge-danger">Критический</span>
    </div>
    <div class="sub-task-counter text-right">
        <span class="badge badge-pill badge-primary">%s из %s</span>
    </div>
</div>




<div class="card p-2 d-flex flex-column task sortable" data-toggle="modal" data-target="#task_editor">
    <div class="task-title">Построить дерево</div>
    <div class="text-right">
        <span class="badge badge-pill badge-dark">7</span>
        <span class="badge badge-warning">Высокий</span>
    </div>
</div>
<div class="card p-2 d-flex flex-column task" data-toggle="modal" data-target="#task_editor">
    <div class="task-title">Посадить сына</div>
    <div class="text-right">
        <span class="badge badge-pill badge-dark">5</span>
        <span class="badge badge-success">Средний</span>
    </div>
</div>
<div class="card p-2 d-flex flex-column task" data-toggle="modal" data-target="#task_editor">
    <div class="task-title">Покушать</div>
    <div class="text-right">
        <span class="badge badge-pill badge-dark">2</span>
        <span class="badge badge-info">Низкий</span>
    </div>
</div>