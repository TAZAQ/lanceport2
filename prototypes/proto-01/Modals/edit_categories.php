<div class="modal fade" id="modal_edit_category" tabindex="-1" role="dialog" aria-labelledby="modal_edit_category_label" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable h-100" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_edit_category_label">Категории</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body container-fluid">
                <div class="row">
                    <div class="col-12 col-md-4 col-xl-3 categories-container d-flex flex-column mb-3">
                        <div class="d-flex flex-row align-content-center justify-content-between">
                            <h6>Управление</h6>
                            <div class="d-flex">
                                <h5 class="mr-1"><i class="fas fa-plus-circle btn-custom text-success" data-toggle="modal" data-target="#modal_new_category"></i></h5>
                                <h5><i class="fas fa-sliders-h text-black-50 btn-custom" id="btn_category_settings"></i></h5>
                            </div>
                        </div>
                        <hr class="m-0">

                        <ul class="list-group" id="id_category_list">
                            <?php include "categories.php"?>
                        </ul>
                    </div>
                    <div class="col category-projects-container d-flex flex-wrap align-content-start mb-3">
                        <?php include "projects_in_categories.php"?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>