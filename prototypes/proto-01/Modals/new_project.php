<div class="modal fade" id="modal_new_project" tabindex="-1" role="dialog" aria-labelledby="NewProjectLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form method="post" class="modal-content modal-box-shadow-2">
            <div class="modal-header">
                <h5 class="modal-title" id="NewProjectLabel">Новый проект</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body container-fluid">
                <div class="row">
                    <div class="col d-flex flex-column">

                        <!-- Название проекта -->
                        <label class="mb-3">
                            <span class="text-primary"><i class="far fa-file"></i> Название проекта</span>
                            <input type="text" class="form-control" name="project_name" maxlength="50" placeholder="Максимум 50 символов">
                        </label>

                        <label class="mb-3 non-select text-primary"><i class="far fa-image"></i> Изображение проекта
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image_file_input" name="task_result_image" aria-describedby="image_file_input">
                                <label class="custom-file-label" for="image_file_input">Выберите файл</label>
                            </div>
                        </label>

                        <!-- Описание проекта -->
                        <label class="mb-3">
                            <span class="non-select text-primary"><i class="far fa-file-alt"></i> Описание проекта</span>
                            <textarea name="project_description" class="form-control" cols="35" rows="5" style="resize: none" placeholder="Максимум 1000 символов"></textarea>
                        </label>

                        <!-- Категория -->
                        <label class="mb-3">
                            <span class="text-primary"><i class="far fa-folder"></i> Категория</span>
                            <select class="form-control" name="project_category">
                                <option value="0">Без категории</option>
                                <option value="1">Категория 1</option>
                                <option value="2">Категория 2</option>
                            </select>
                        </label>

                        <!-- Сроки -->
                        <span class="non-select text-primary"><i class="far fa-calendar"></i> Сроки</span>
                        <div class="row">
                            <div class="col-6 d-flex flex-column">
                                <label>Начало<input type="date" class="form-control mb-2" name="project_date_start"></label>
                            </div>
                            <div class="col-6 d-flex flex-column">
                                <label>Окончание<input type="date" class="form-control" name="project_date_finish"></label>
                            </div>
                        </div>

                        <!-- Метки -->
                        <label class="mb-3">
                            <span class="non-select text-primary"><i class="fas fa-tags"></i> Метки</span>
                            <textarea name="project_tags" class="form-control" cols="35" rows="5" style="resize: none" placeholder="Метки через запятую"></textarea>
                        </label>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-outline-success">Создать</button>
            </div>
        </form>
    </div>
</div>