<div class="modal fade" id="modal_new_category" tabindex="-1" role="dialog" aria-labelledby="modal_new_category_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-box-shadow-2">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_new_category_label">Новая категория</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Название категории -->
                <div class="task-change-field mb-3">
                    <h6 class="non-select text-primary"><i class="far fa-file-alt"></i> Название</h6>
                    <input type="text" class="form-control" id="id_category_name" maxlength="50" placeholder="Максимум 50 символов">
                </div>
                <!-- Описание категории -->
                <div class="task-change-field mb-3">
                    <h6 class="non-select text-primary"><i class="far fa-file-alt"></i> Описание</h6>
                    <textarea name="task_result_text" id="id_category_descr" class="form-control" maxlength="100" cols="35" rows="5" style="resize: none" placeholder="Максимум 100 символов"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-outline-success" id="add_category" data-dismiss="modal">Добавить</button>
            </div>
        </div>
    </div>
</div>