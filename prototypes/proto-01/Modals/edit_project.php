<div class="modal fade" id="modal_edit_project" tabindex="-1" role="dialog" aria-labelledby="NewProjectLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="NewProjectLabel">Редактирование проекта</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body container-fluid">
                <div class="row">
                    <div class="col d-flex flex-column">

                        <!-- Название проекта -->
                        <label class="mb-3">
                            <span class="text-primary"><i class="far fa-file"></i> Название проекта</span>
                            <input type="text" class="form-control" name="project_name" maxlength="50" placeholder="Максимум 50 символов">
                        </label>

                        <!-- Описание проекта -->
                        <label class="mb-3">
                            <span class="non-select text-primary"><i class="far fa-file-alt"></i> Описание проекта</span>
                            <textarea name="project_description" class="form-control" cols="35" rows="5" style="resize: none" placeholder="Максимум 1000 символов"></textarea>
                        </label>

                        <!-- Категория -->
                        <label class="mb-3">
                            <span class="text-primary"><i class="far fa-folder"></i> Категория</span>
                            <select class="form-control" name="project_category">
                                <option value="0">Без категории</option>
                                <option value="1">Категория 1</option>
                                <option value="2">Категория 2</option>
                            </select>
                        </label>

                        <!-- Сроки -->
                        <span class="non-select text-primary"><i class="far fa-calendar"></i> Сроки</span>
                        <div class="d-flex flex-row">
                            <div class="d-flex flex-grow-1 pl-0">
                                <label>Начало<input type="date" class="form-control mb-2" name="project_date_start"></label>
                            </div>
                            <div class="d-flex flex-grow-1 pr-0">
                                <label>Окончание<input type="date" class="form-control" name="project_date_finish"></label>
                            </div>
                        </div>

                        <!-- Метки -->
                        <label class="mb-3">
                            <span class="non-select text-primary"><i class="fas fa-tags"></i> Метки</span>
                            <textarea name="project_tags" class="form-control" cols="35" rows="5" style="resize: none" placeholder="Метки через запятую"></textarea>
                        </label>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="container-fluid d-flex justify-content-between">
                    <div class="d-flex">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Удалить</button>
                    </div>
                    <div>
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-outline-success" id="btn_task_save">Изменить</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>