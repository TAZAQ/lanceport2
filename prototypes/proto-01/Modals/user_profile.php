<div class="modal fade" id="user_profile" tabindex="-1" role="dialog" aria-labelledby="profileLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="profile-form" action="" method="post">
                <div class="modal-header">
                    <h4 class="modal-title" id="profileLabel">Личный кабинет</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body container-fluid" id="user_data">
                    <div class="row">
                        <div class="col-5 border-right">
                            <div class="d-flex flex-column pl-3 pr-3 align-items-center">
                                <h5>Параметры учётной записи</h5>
                                <hr class="mt-0" style="width: 100%">
                                <label>
                                    <span class="text-primary">Логин</span>
                                    <input type="text" class="form-control mr-2" name="user_name" placeholder="Логин для входа">
                                </label>
                                <label>
                                    <span class="text-primary">Старый пароль</span>
                                    <input type="text" class="form-control mr-2" name="user_old_pass" placeholder="Введите старый пароль">
                                </label>
                                <label>
                                    <span class="text-primary">Новый пароль</span>
                                    <input type="text" class="form-control mr-2" name="user_new_pass" placeholder="Введите новый пароль">
                                </label>
                                <label>
                                    <span class="text-primary">Повторите пароль</span>
                                    <input type="text" class="form-control mr-2" name="user_new_pass2" placeholder="Повторите новый пароль">
                                </label>
                            </div>
                        </div>
                        <div class="col-7 d-flex flex-column">
                            <div class="d-flex flex-column mb-4">
                                <img class="user-avatar rounded-circle my-border cursor-select align-self-center" id="avatar_image"
                                     src="Img/avatar.jpg" alt="" width="100px" height="100px">

                                <div class="user-name text-center">
                                    {$user_full_name}
                                </div>
                            </div>
                            <label>
                                <span class="text-primary">ФИО</span>
                                <input type="text" class="form-control" name="user_full_name" placeholder="ФИО для чата">
                            </label>
                            <label>
                                <span class="text-primary">Инициалы</span>
                                <input type="text" class="form-control" name="user_initials" placeholder="Инициалы">
                            </label>
                            <label>
                                <span class="text-primary">E-mail</span>
                                <input type="text" class="form-control" name="user_email" placeholder="E-mail для связи">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-success">Готово</button>
                </div>
            </form>
        </div>
    </div>
</div>