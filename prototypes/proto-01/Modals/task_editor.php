<div class="modal fade" id="task_editor" tabindex="-1" role="dialog" aria-labelledby="task_editor_label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <form class="modal-content" id="task_change_form">
            <div class="modal-header">
                <!-- Текст задачи -->
                <input type="text" class="col-4 modal-title form-control border-0" id="task_editor_label" name="task_name" value="Вырастить дом">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body container-fluid">
                <div class="row">
                    <div class="col-4 border-right">
                        <!-- Описание -->
                        <div class="task-change-field mb-3">
                            <h6 class="non-select text-primary"><i class="far fa-file-alt"></i> Описание</h6>
                            <input type="text" class="form-control" name="task_description" placeholder="Максимум 100 символов">
                        </div>

                        <!-- Исполнитель -->
                        <div class="task-change-field mb-3">
                            <h6 class="non-select text-primary"><i class="far fa-user"></i> Исполнитель</h6>
                            <select class="form-control" name="worker_selector" id="worker_selector_id">
                                <option value="0">Worker1</option>
                                <option value="1">Worker2</option>
                                <option value="2">Worker3</option>
                                <option value="3">Worker4</option>
                            </select>
                        </div>

                        <!-- Сроки -->
                        <div class="task-change-field mb-3">
                            <h6 class="non-select text-primary"><i class="far fa-calendar"></i> Сроки</h6>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-6 pl-0">
                                        <span>Начало</span>
                                        <input type="date" class="form-control mb-2" name="date_start">
                                    </div>
                                    <div class="col-6 pr-0">
                                        <span>Окончание</span>
                                        <input type="date" class="form-control" name="date_finish">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Приоритет -->
                        <div class="task-change-field mb-3">
                            <h6 class="non-select text-primary"><i class="fas fa-signal"></i> Приоритет</h6>
                            <select class="form-control" name="priority_selector" id="priority_selector_id">
                                <option value="0">Низкий</option>
                                <option value="1">Средний</option>
                                <option value="2">Высокий</option>
                                <option value="3">Критический</option>
                            </select>
                        </div>

                        <!-- Предыдущая задача -->
                        <div class="task-change-field mb-3">
                            <h6 class="non-select text-primary"><i class="fas fa-sitemap"></i> Предыдущая задача</h6>
                            <select class="form-control" name="hierarchy_selector" id="hierarchy_selector_id">
                                <option value="">Корень</option>
                                <option value="0">Задача1</option>
                                <option value="1">Задача2</option>
                                <option value="2">Задача3</option>
                                <option value="3">Задача4</option>
                            </select>
                        </div>

                        <!-- Затраты -->
                        <div class="task-change-field mb-3">
                            <h6 class="non-select text-primary"><i class="far fa-clock"></i> Затраты (часы)</h6>
                            <input type="text" class="form-control" name="task_estimate" value="1.00" placeholder="Например: 1.00">
                        </div>
                    </div>


                    <div class="col-4 border-right">
                        <!-- Подзадачи -->
                        <div class="d-flex flex-column task-change-field mb-3 h-100">
                            <div class="d-flex flex-row align-content-center justify-content-between">
                                <div class="d-flex flex-row">
                                    <h6 class="non-select text-primary mr-1"><i class="fas fa-tasks"></i> Подзадачи </h6>
                                    <h5><i class="fas fa-plus-circle btn-custom text-success" id="addSubTask"></i></h5>
                                </div>
                                <h5><i class="fas fa-sliders-h text-black-50 btn-custom" id="btn_subtasks_settings"></i></h5>
                            </div>

                            <div class="subtasks-container" id="subtasks_container">
                                <?php include "subtasks.php"?>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <!-- Изображение результата -->
                        <div class="task-change-field mb-3">
                            <h6 class="non-select text-primary"><i class="far fa-image"></i> Изображение результата</h6>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image_file_input" name="task_result_image" aria-describedby="image_file_input">
                                <label class="custom-file-label" for="image_file_input">Выберите файл</label>
                            </div>
                        </div>

                        <!-- Описание результата -->
                        <div class="task-change-field mb-3">
                            <h6 class="non-select text-primary"><i class="far fa-file-alt"></i> Описание результата</h6>
                            <textarea name="task_result_text" id="id_task_result_text" class="form-control" cols="35" rows="5" style="resize: none"></textarea>
                        </div>

                        <!-- Вывести задачу в отчёт -->
                        <div class="task-change-field mb-3">
                            <h6 class="non-select text-primary"><i class="fas fa-file"></i> Отчёт</h6>
                            <label><input type="checkbox" class="custom-checkbox" name="task_in_report"> Показать задачу в отчёте</label>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="container-fluid d-flex justify-content-between">
                    <div class="d-flex">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Удалить</button>
                    </div>
                    <div>
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-outline-success" id="btn_task_save">Сохранить</button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>