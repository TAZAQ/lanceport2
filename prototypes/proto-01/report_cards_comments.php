<div class="list-group">
    <div class="list-group-item list-group-item-action flex-column post_comment" id="1">
        <div class="d-flex w-100 justify-content-between">
            <div class="mb-2 text-primary">
                <img class="rounded-circle my-border mr-1 cursor-select"
                     src="Img/avatar.jpg" alt="" width="38px" height="38px"
                     data-toggle="modal" data-target="#user_profile"
                >
                Владислав Чеснов
            </div>
            <small>30 дек в 22:43</small>
        </div>
        <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius
            blandit.</p>
        <div class="text-right"><small class="text-muted">Ответить</small></div>
    </div>

    <div class="list-group-item list-group-item-action flex-column post_comment" id="2">
        <div class="d-flex w-100 justify-content-between">
            <div class="mb-2 text-primary">
                <img class="rounded-circle my-border mr-1 cursor-select"
                     src="Img/avatar.jpg" alt="" width="38px" height="38px"
                     data-toggle="modal" data-target="#user_profile"
                >
                Владислав Чеснов
            </div>
            <small class="text-muted">3 янв в 22:34</small>
        </div>
        <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius
            blandit.</p>
        <div class="text-right"><small class="text-muted">Ответить</small></div>
    </div>

    <div class="list-group-item list-group-item-action flex-column post_comment" id="3">
        <div class="d-flex w-100 justify-content-between">
            <div class="mb-2 text-primary">
                <img class="rounded-circle my-border mr-1 cursor-select"
                     src="Img/avatar.jpg" alt="" width="38px" height="38px"
                     data-toggle="modal" data-target="#user_profile"
                >
                Владислав Чеснов
            </div>
            <small class="text-right text-muted">23 янв в 22:06</small>
        </div>

        <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius
            blandit.</p>
        <div class="text-right"><small class="text-muted">Ответить</small></div>
    </div>
</div>