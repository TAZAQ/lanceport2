<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>proto-01</title>
    <link rel="stylesheet" href="Styles/bootstrap.min.css">
    <link rel="stylesheet" href="Styles/lanceport.css">
    <link rel="stylesheet" href="Styles/font-awesome/css/all.min.css">
</head>
<body>
<header>
    <div class="container-fluid p-2">
        <div class="d-flex justify-content-between">
            <div class="row pl-3 pr-3">
                <div class="dropdown mr-2">
                    <button class="btn btn-light dropdown-toggle"
                            type="button" id="dropdownCategories" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        Выбрать проект
                    </button>
                    <div class="dropdown-menu prjs-container prj-dropdown pt-3 pb-3 pr-0 shadow-lg"
                         aria-labelledby="dropdownCategories">
                        <div class="d-flex flex-column justify-content-center p-3">
                            <button class="btn btn-outline-secondary mb-2" data-toggle="modal" data-target="#modal_edit_category">Настроить категории</button>
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#modal_new_project">Создать проект</button>
                        </div>
                        <hr class="m-3">
                        <div class="container-fluid list-group">
                            <?php include "projects.php" ?>
                        </div>
                    </div>
                </div>
                <div class="dropdown mr-2">
                    <button class="btn btn-outline-light dropdown-toggle"
                            type="button" id="dropdownSorts" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        Без сортировки
                    </button>
                    <div class="dropdown-menu prjs-container prj-dropdown shadow-lg"
                         aria-labelledby="dropdownSorts">
                        <?php include "sorts.php"?>
                    </div>
                </div>
                <div class="dropdown mr-2">
                    <button class="btn btn-outline-light dropdown-toggle"
                            type="button" id="dropdownFilters" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        Фильтры
                    </button>
                    <div class="dropdown-menu prjs-container prj-dropdown shadow-lg"
                         aria-labelledby="dropdownFilters">
                        <?php include "filters.php" ?>
                    </div>
                </div>
            </div>
            <div class="navbar-brand text-light d-flex flex-row">
                <span class="mr-2">Название проекта</span>
                <i class="btn btn-outline-light fas fa-cog" data-toggle="modal" data-target="#modal_edit_project"></i>
            </div>
            <div>
                <img class="rounded-circle my-border mr-3 cursor-select"
                     src="Img/avatar.jpg" alt="" width="38px" height="38px"
                     data-toggle="modal" data-target="#user_profile"
                >
                <div class="btn btn-outline-light">Выход</div>
            </div>
        </div>
    </div>
</header>

<main class="container-fluid">
    <div class="row" id="states-container">
        <div class="col-sm-12 col-md-5 col-lg-3 col-xl-2 card m-2 states-style pt-3 pb-2">
            <div class="card-title">
                <div class="d-flex">
                    <h5 class="non-select mr-1">Задачи проекта</h5>
                    <span class="mr-2 text-info" id="sortable1_counter">[40]</span>
                    <h4><i class="fas fa-plus-circle btn-custom text-success" id="addTask"></i></h4>
                </div>
                <hr class="m-0">
            </div>
            <div id="sortable1" class="card border-0 state-tasks connectedSortable">
                <?php include "tasks.php" ?>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-3 col-xl-2 card state-card m-2 states-style pt-3 pb-2">
            <div class="card-title">
                <div class="d-flex">
                    <h5 class="non-select mr-1">Сделаю</h5>
                    <span class="mr-2 text-info" id="sortable2_counter">[20]</span>
                </div>
                <hr class="m-0">
            </div>
            <div id="sortable2" class="card border-0 state-tasks connectedSortable">
                <div class="card p-2 d-flex flex-column task">
                    <div>Задача 0</div>
                    <div class="text-right">
                        <span class="badge badge-pill badge-dark">2</span>
                        <span class="badge badge-info">Низкий</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-3 col-xl-2 card state-card m-2 states-style pt-3 pb-2">
            <div class="card-title">
                <div class="d-flex">
                    <h5 class="non-select mr-1">В процессе</h5>
                    <span class="mr-2 text-info" id="sortable3_counter">[10]</span>
                </div>
                <hr class="m-0">
            </div>
            <div id="sortable3" class="card border-0 state-tasks connectedSortable">
                <div class="card p-2 d-flex flex-column task">
                    <div>Задача 00</div>
                    <div class="text-right">
                        <span class="badge badge-pill badge-dark">2</span>
                        <span class="badge badge-info">Низкий</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-3 col-xl-2 card state-card m-2 states-style pt-3 pb-2">
            <div class="card-title">
                <div class="d-flex">
                    <h5 class="non-select mr-1">Тестирование</h5>
                    <span class="mr-2 text-info" id="sortable4_counter">[5]</span>
                </div>
                <hr class="m-0">
            </div>
            <div id="sortable4" class="card border-0 state-tasks connectedSortable">
                <div class="card p-2 d-flex flex-column task">
                    <div>Задача 000</div>
                    <div class="text-right">
                        <span class="badge badge-pill badge-dark">2</span>
                        <span class="badge badge-info">Низкий</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-3 col-xl-2 card state-card m-2 states-style pt-3 pb-2">
            <div class="card-title">
                <div class="d-flex">
                    <h5 class="non-select mr-1">Готово</h5>
                    <span class="mr-2 text-info" id="sortable5_counter">[2]</span>
                </div>
                <hr class="m-0">
            </div>
            <div id="sortable5" class="card border-0 state-tasks connectedSortable">
                <div class="card p-2 d-flex flex-column task">
                    <div>Задача 0000</div>
                    <div class="text-right">
                        <span class="badge badge-pill badge-dark">2</span>
                        <span class="badge badge-info">Низкий</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<!-- Modal edit categories -->
<?php include "Modals/edit_categories.php"?>

<!-- Modal new category -->
<?php include "Modals/new_category.php"?>

<!-- Modal User Profile modal -->
<?php include "Modals/user_profile.php" ?>

<!-- Modal Task editor -->
<?php include "Modals/task_editor.php" ?>

<!-- Modal new project -->
<?php include "Modals/new_project.php" ?>

<!-- Modal edit project -->
<?php include "Modals/edit_project.php" ?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Bootstrap JS -->
<!-- https://github.com/lukasoppermann/html5sortable -->
<script src="Scripts/jquery-3.4.1.min.js"></script>
<script src="Scripts/html5sortable.min.js"></script>
<script src="Scripts/bootstrap.bundle.min.js"></script>
<script src="Scripts/lanceport.js"></script>
</body>
</html>