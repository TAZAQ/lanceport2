<a href="/projects/lanceport" class="card project-card d-flex flex-column m-1 a-non-decor">
    <div class="block-project-icon"><span class="avatar">LPA</span></div>
    <div class="block-project-name d-flex flex-column justify-content-center text-center">
        <span class="small-text">Lanceport production automation</span>
    </div>
</a>

<div class="btn btn-outline-success m-1 d-flex flex-column align-items-center justify-content-center btn-add-project" data-toggle="modal" data-target="#modal_new_project">
    <i class="fas fa-plus"></i>
</div>