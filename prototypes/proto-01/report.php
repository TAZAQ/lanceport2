<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>proto-01</title>
    <link rel="stylesheet" href="Styles/bootstrap.min.css">
    <link rel="stylesheet" href="Styles/lanceport.css">
    <link rel="stylesheet" href="Styles/font-awesome/css/all.min.css">
</head>
<body>
<header>
    <div class="container-fluid p-2">
        <div class="d-flex justify-content-between">
            <div class="row pl-3 pr-3">
                <div class="dropdown mr-2">
                    <button class="btn btn-light dropdown-toggle"
                            type="button" id="dropdownCategories" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        Выбрать проект
                    </button>
                    <div class="dropdown-menu prjs-container prj-dropdown pt-3 pb-3 pr-0 shadow-lg"
                         aria-labelledby="dropdownCategories">
                        <div class="d-flex flex-column justify-content-center p-3">
                            <button class="btn btn-outline-secondary mb-2" data-toggle="modal" data-target="#modal_edit_category">Настроить категории</button>
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#modal_new_project">Создать проект</button>
                        </div>
                        <hr class="m-3">
                        <div class="container-fluid list-group">
                            <?php include "projects.php" ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="navbar-brand text-light d-flex flex-row">
                <span class="mr-2">Название проекта</span>
                <i class="btn btn-outline-light fas fa-cog" data-toggle="modal" data-target="#modal_edit_project"></i>
            </div>
            <div>
                <img class="rounded-circle my-border mr-3 cursor-select"
                     src="Img/avatar.jpg" alt="" width="38px" height="38px"
                     data-toggle="modal" data-target="#user_profile"
                >
                <div class="btn btn-outline-light">Выход</div>
            </div>
        </div>
    </div>
</header>

<main class="container-fluid mt-3 mb-3">
    <div class="row">
        <div class="col"></div>
        <div class="col-5">
            <?php include "report_cards.php"; ?>
            <?php include "report_cards.php"; ?>
        </div>
        <div class="col"></div>
    </div>
</main>


<!-- Modal edit categories -->
<?php include "Modals/edit_categories.php"?>

<!-- Modal new category -->
<?php include "Modals/new_category.php"?>

<!-- Modal User Profile modal -->
<?php include "Modals/user_profile.php" ?>

<!-- Modal new project -->
<?php include "Modals/new_project.php" ?>

<!-- Modal edit project -->
<?php include "Modals/edit_project.php" ?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Bootstrap JS -->
<!-- https://github.com/lukasoppermann/html5sortable -->
<script src="Scripts/jquery-3.4.1.min.js"></script>
<script src="Scripts/html5sortable.min.js"></script>
<script src="Scripts/bootstrap.bundle.min.js"></script>
<script src="Scripts/lanceport_report.js"></script>
</body>
</html>