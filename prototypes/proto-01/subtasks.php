<!-- Сама подзадача -->
<div class="input-group mb-1 subtask" draggable="true" role="option" aria-grabbed="false">
    <div class="input-group-prepend">
        <div class="input-group-text"><input type="checkbox" name="subtask_checkbox"></div>
    </div>
    <input type="text" class="form-control" name="subtask_text" value="Новая подзадача">
    <div class="d-flex subtasks_settings hidden">
        <div class="btn btn-outline-danger btn_subtask_del mr-1">✕</div>
        <div class="d-flex align-items-center btn_subtask_drag"><i class="fas fa-grip-lines text-black-50"></i></div>
    </div>
</div>