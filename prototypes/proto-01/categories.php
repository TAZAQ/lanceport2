<li class="list-group-item d-flex justify-content-between align-items-center category_li active" id="0">
    Без категории
</li>

<li class="list-group-item d-flex justify-content-between align-items-center category_li" id="2">
    Категория 1
    <span class="badge badge-danger badge-pill hidden btn_category_del non-select">&times;</span>
</li>

<li class="list-group-item d-flex justify-content-between align-items-center category_li" id="3">
    Категория 2
    <span class="badge badge-danger badge-pill hidden btn_category_del non-select">&times;</span>
</li>

<li class="list-group-item d-flex justify-content-between align-items-center category_li" id="4">
    Категория 3
    <span class="badge badge-danger badge-pill hidden btn_category_del non-select">&times;</span>
</li>