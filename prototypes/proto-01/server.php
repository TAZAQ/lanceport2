<?php
$data = file_get_contents("php://input");
$data = json_decode($data);

if ($data->action === "new_task") {
    echo json_encode(["data" => "
<div class=\"card p-2 d-flex flex-column task\" draggable=\"true\" role=\"option\" aria-grabbed=\"false\" data-toggle=\"modal\" data-target=\"#task_editor\">
    <div class=\"task-title\">Новая задача {$data->temp_id}</div>
</div>"
    ]);
}
elseif ($data->action === "new_category") {
    echo json_encode(["data" => "            
            <li class=\"list-group-item d-flex justify-content-between align-items-center category_li\" id=\"$data->temp_id\" title=\"$data->category_descr\">
                $data->category_name
                <span class=\"badge badge-danger badge-pill hidden btn_category_del non-select\">&times;</span>
            </li>"
    ]);
}
else {
    echo json_encode(["data" => "Wrong action"]);
}