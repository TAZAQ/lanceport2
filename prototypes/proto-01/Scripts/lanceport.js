// global variables
// счётчик задач
let int_temp_task_counter = +1;
// флаг настройки подзадач
let bool_is_subtask_setting = false;
// флаг настройки категорий
let bool_is_category_setting = false;
// текущая категория
let int_cur_category = 0;

// точка входа
main();

function main() {
    // слушатели на столбики
    creatingSortableStatesListeners();
    // счётчики на столбики
    recountingTasksInStates();
    // новая задача
    addNewTask();
    // новая подзача + удаление
    addListenersToSubTask();
    // сортировка на подзадачи
    creatingSortableSubTaskListeners();
    // настройки подзадач
    subtaskSettings();
    // новая категория + удаление
    addListenersToCategories();
    // настройки категорий
    categorySettings();

    // показать картинку реальног размера
    postImageShowReal();
}


// слушатели на столбики
function creatingSortableStatesListeners() {
    // drag and drop
    sortable("#sortable1, #sortable2, #sortable3, #sortable4, #sortable5", {
            acceptFrom: "#sortable1, #sortable2, #sortable3, #sortable4, #sortable5",
            forcePlaceholderSize: true,
            placeholderClass: 'ph-class',
            hoverClass: 'border-primary'
        }
    );

    // вешаю обновление счётчиков
    for (let sortable_item = 0; sortable_item < 5; sortable_item++) {
        // sortable event Listener adding
        sortable("#sortable1, #sortable2, #sortable3, #sortable4, #sortable5")[sortable_item].addEventListener('sortstop', function (e) {
            let target = e.target;
            let origin = e.detail.origin.container;
            recountingTasksInStates(undefined, target, origin);
        });
    }
}

// счётчики на столбики
function recountingTasksInStates(state_count=5, target, origin) {
    // пересчет счётчиков, ситуации:
    // 1) target и origin !== undefined, то пересчитывается счётчик target и origin
    // 2) если, то идёт пересчёт счётчиков === 0...state_count
    if (target !== undefined && origin !== undefined) {
        $(`#${target.id}_counter`).text(`[${target.childElementCount}]`);
        $(`#${origin.id}_counter`).text(`[${origin.childElementCount}]`);
    } else {
        for (let sortable_item = 0; sortable_item < state_count; sortable_item++) {
            let cur_sortable = '#sortable' + (sortable_item + 1);
            let cur_sortable_items = $(cur_sortable)[0].childElementCount;
            $(`${cur_sortable}_counter`).text(`[${cur_sortable_items}]`);
        }
    }

}

// новая задача
function addNewTask() {
    // новая задача, fetch на сервер, для создания "пусто" задачи
    addTask.onclick = async () => {
        let my_data = {"action": "new_task", "temp_id": int_temp_task_counter};

        let response = await fetch('server.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(my_data)
        });

        let result = await response.json();
        $('#sortable1').append(result.data);
        int_temp_task_counter++;

        // пересчитываем количество задач для первого столбика
        recountingTasksInStates(1);
    };
}

// новая подзача + удаление
function addListenersToSubTask() {
    // счётчик на задачи
    let subtask_count = 1;

    // удаление
    function delSubtaskListener() {
        $('.btn_subtask_del').on("click", function () {
            $(this).parents('.subtask').remove();
        });
    }

    // добавление
    addSubTask.onclick = () => {
        let classHidden = "hidden";
        if (bool_is_subtask_setting) classHidden = "";

        $('#subtasks_container').append(`
            <div class="input-group mb-1 subtask" draggable="true" role="option" aria-grabbed="false">
                <div class="input-group-prepend">
                    <div class="input-group-text"><input type="checkbox" name="subtask_checkbox"></div>
                </div>
                <input type="text" class="form-control" name="subtask_text" value="Новая подзадача ${subtask_count}">
                <div class="d-flex subtasks_settings ${classHidden}">
                    <div class="btn btn-outline-danger btn_subtask_del mr-1">✕</div>
                    <div class="d-flex align-items-center btn_subtask_drag" title="Тащи меня, Сееееенпай!"><i class="fas fa-grip-lines text-black-50"></i></div>
                </div>
            </div>
        `
        );
        subtask_count++;

        // удаление
        delSubtaskListener();
    };

    // удаление при первой загрузке
    delSubtaskListener();
}

// drag and drop на контейнер подазадач
function creatingSortableSubTaskListeners() {
    sortable('#subtasks_container', {
        forcePlaceholderSize: true,
        placeholderClass: 'ph-class',
        hoverClass: 'border-primary'
    });
}

// настройки подзадач
function subtaskSettings() {
    $('#btn_subtasks_settings').on("click", function() {
        bool_is_subtask_setting = !bool_is_subtask_setting;

        if ($('.subtasks_settings').hasClass("hidden")) {
            $('.subtasks_settings').removeClass("hidden");
        } else {
            $('.subtasks_settings').addClass("hidden");
        }
    });
}


// активация категории
function addListenersToCategories() {

    // добавление/удаление активности к категории
    function categoryActivate() {
        $(".category_li").on("click", function () {
            $('.category_li').removeClass('active');
            $(this).addClass("active");

            int_cur_category = this.id;
        });
    }

    // удаление
    function delCategoryListener() {
        $('.btn_category_del').on("click", function () {
            $(this).parents('.category_li').remove();
        });
    }

    // добавление
    add_category.onclick = async () => {
        let classHidden = "hidden";
        if (bool_is_category_setting) classHidden = "";

        let category_name = $('#id_category_name').val();
        let category_descr = $('#id_category_descr').val();

        let my_data = {
            "action": "new_category",
            "temp_id": 100,
            "category_name": category_name,
            "category_descr": category_descr
        };

        let response = await fetch('server.php', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(my_data)
        });

        let result = await response.json();
        $('#id_category_list').append(result.data);

        // удаление
        delCategoryListener();

        // активация категории
        categoryActivate();
    };

    // удаление при первой загрузке
    delCategoryListener();
    // активация при первой закгрузке
    categoryActivate();
}

// настройки категорий
function categorySettings() {
    $('#btn_category_settings').on("click", function() {
        bool_is_category_setting = !bool_is_category_setting;

        if ($('.btn_category_del').hasClass("hidden")) {
            $('.btn_category_del').removeClass("hidden");
        } else {
            $('.btn_category_del').addClass("hidden");
        }
    });
}

function postImageShowReal() {
    let a = 10;
    $('.post_main_img').click(function () {
        document.append("hello");
    });
}
