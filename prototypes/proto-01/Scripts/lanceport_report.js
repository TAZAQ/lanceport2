// точка входа
main();


function main() {
    // показать картинку реальног размера
    postImageShowReal();

    // ответка
    answerPanelShow();

    // добавить ответ
    addPostAnswer();
}

function postImageShowReal() {
    $('.post_main_img').click(function () {
        open(this.src, "_blank")
    });
}

function answerPanelShow() {
    $(".post_comment").click(function () {
        let comment_parent_id = this.id;

        if (! $(this).hasClass("answerPanelShowed")) {
            $(this).addClass("answerPanelShowed");

            $(this).append(
                `        
                    <div class="d-flex answer_panel ml-3">
                        <img class="rounded-circle my-border mr-1 cursor-select"
                             src="Img/avatar.jpg" alt="" width="38px" height="38px"
                             data-toggle="modal" data-target="#user_profile"
                        >
                        <input class="form-control mr-1" type="text" placeholder="Написать комментарий... " id="${comment_parent_id}">
                        <button class="btn btn-outline-primary send_post_answer"><i class="fas fa-angle-double-right"></i></button>
                    </div>
                `
            );
        }
    });
}

function addPostAnswer() {
    $(".send_post_answer").click(function () {
        let text_input = $(this).siblings(".post_answer")[0];
        let message_text = text_input.value;

        // меньше трёх символов - выходим
        if (message_text.length <= 3) return;

        // нужный контейнер с комментариями
        let post_comments_container =
            $(this)
                .parent(".answer_panel")
                .parent(".post_answer_group")
                .siblings(".post_comments")
                .children(".list-group")
        ;

        let now_date = new Date();
        let now_date_options = {
            day: 'numeric',
            month: 'long',
            // year: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
        };
        now_date = now_date.toLocaleString("ru", now_date_options);

        // TODO: заменить на функцию
        let new_comment_id = 11;

        // добавляем сообщение
        post_comments_container.append(
            `
                <div class="list-group-item list-group-item-action flex-column post_comment" id="${new_comment_id}">
                    <div class="d-flex w-100 justify-content-between">
                        <div class="mb-2 text-primary">
                            <img class="rounded-circle my-border mr-1 cursor-select"
                                 src="Img/avatar.jpg" alt="" width="38px" height="38px"
                                 data-toggle="modal" data-target="#user_profile"
                            >
                            Владислав Чеснов
                        </div>
                        <small>${now_date}</small>
                    </div>
                    <p class="mb-1">${message_text}</p>
                    <div class="text-right"><small class="text-muted">Ответить</small></div>
            
                </div>
            `
        );

        // очистка поля
        text_input.value = "";

        answerPanelShow();
    });
}