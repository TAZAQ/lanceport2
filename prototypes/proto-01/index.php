<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>proto-01</title>
    <link rel="stylesheet" href="Styles/bootstrap.min.css">
    <link rel="stylesheet" href="Styles/lanceport.css">
</head>
<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 col-md-2 col-lg-3 col-xl-4"></div>
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-4 p-5">
                <div class="d-flex justify-content-center">
                    <div class="display-4 text-secondary non-select">free</div>
                    <div class="display-1 text-monospace non-select">Lance</div>
                </div>

                <div class="card shadow-lg p-2 pl-4 pr-4">
                    <div class="card-body">
                        <h4 class="d-flex justify-content-center text-secondary">Добро пожаловать</h4>
                        <hr>
                        <form method="post" class="d-flex justify-content-center flex-column">
                            <label>Логин / E-mail
                                <input type="text" name="login" class="form-control mb-1">
                            </label>
                            <label>Пароль
                                <input type="password" name="password" class="form-control mb-4">
                            </label>
                            <input type="submit" value="Войти" class="btn btn-success mb-2">
                        </form>
                        <hr>
                        <div class="d-flex flex-row justify-content-around">
                            <a href="" data-toggle="modal" data-target="#contacts">
                                Контакты
                            </a>
                            <a href="" data-toggle="modal" data-target="#feedback">
                                Обратная связь
                            </a>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="display-4 text-secondary non-select">re</div>
                    <div class="display-1 text-monospace non-select">Port</div>
                </div>
            </div>
            <div class="col-sm-1 col-md-2 col-lg-3 col-xl-4 d-flex flex-column justify-content-end"></div>
        </div>
    </div>

    <!-- Contacts modal -->
    <div class="modal fade" id="contacts" tabindex="-1" role="dialog" aria-labelledby="feedbackLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="feedback-form" action="" method="post">
                    <div class="modal-header">
                        <span>По всем вопросам</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body d-flex flex-column">
                        <span>E-mail: <a href="mailto:tazaqsp@mail.ru">tazaqsp@mail.ru</a></span>
                    </div>
                    <div class="modal-footer"></div>
                </form>
            </div>
        </div>
    </div>

    <!-- Feedback modal -->
    <div class="modal fade" id="feedback" tabindex="-1" role="dialog" aria-labelledby="feedbackLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="feedback-form" action="" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="feedbackLabel">Свяжитесь с нами</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body d-flex flex-column">
                        <label>Имя*
                            <input type="text" name="from_name" class="form-control mb-1" required>
                        </label>
                        <label>E-mail*
                            <input type="email" name="from_email" class="form-control mb-1" required>
                        </label>
                        <label>Текст сообщения*
                            <textarea class="form-control" name="from_message" rows="7" required ></textarea>
                        </label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-outline-success">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Bootstrap JS -->
    <script src="Scripts/jquery-3.3.1.slim.min.js"></script>
    <script src="Scripts/bootstrap.bundle.min.js"></script>
</body>
</html>