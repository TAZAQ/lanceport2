<div class="card mb-3">
    <div class="card-header">
        <img class="rounded-circle my-border mr-1 cursor-select"
             src="Img/avatar.jpg" alt="" width="38px" height="38px"
             data-toggle="modal" data-target="#user_profile"
        >
        Владислав Чеснов
    </div>
    <div class="card-body">

        <h4 class="card-title">Special title treatment</h4>
        <p class="card-text">
            With supporting text below as a natural lead-in to additional content.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium blanditiis ea esse id inventore
            ipsam iure nisi pariatur recusandae. Consectetur cupiditate illum iusto pariatur rem, repudiandae sit
            soluta veritatis.
        </p>

        <img class="post_main_img" src="Img/post_img.jpg" alt="Special title treatment">
    </div>
    <hr class="m-1">

    <div class="card-body post_comments">
        <?php include "report_cards_comments.php"?>
    </div>

    <div class="card-body post_answer_group">
        <div class="d-flex answer_panel">
            <img class="rounded-circle my-border mr-1 cursor-select"
                 src="Img/avatar.jpg" alt="" width="38px" height="38px"
                 data-toggle="modal" data-target="#user_profile"
            >
            <input class="form-control mr-1 post_answer" type="text" placeholder="Написать комментарий...">
            <button class="btn btn-outline-primary send_post_answer"><i class="fas fa-angle-double-right"></i></button>
        </div>
    </div>
</div>